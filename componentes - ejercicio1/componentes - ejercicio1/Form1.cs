﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace componentes___ejercicio1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            pictureBox1.Width++;
            pictureBox1.Height++;

            if(pictureBox1.Width==510 && pictureBox1.Height == 515)
            {
                timer1.Enabled=false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            timer1.Enabled=true; //En el caso de que quieras controlarlo por medio de un botón
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            timer1.Enabled = false;
        }
    }
}
