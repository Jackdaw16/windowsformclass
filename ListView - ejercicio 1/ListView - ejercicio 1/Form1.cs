﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ListView___ejercicio_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                listView1.Items.Add(new ListViewItem(textBox1.Text, listView1.Groups[0])); 
            }
            if (radioButton2.Checked)
            {
                listView1.Items.Add(new ListViewItem(textBox1.Text, listView1.Groups[1]));
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            listView1.Items.Add(new ListViewItem("Manzana", listView1.Groups[0]));
        }
    }
}
