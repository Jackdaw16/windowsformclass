﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cliente
{
    public partial class chat : Form
    {
        Socket cliente;
        EndPoint endPointLocal, endPointRemota;
        public chat()
        {
            InitializeComponent();

            cliente = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            cliente.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);

        }

        private String GetLocalIp()
        {
            return "127.0.0.1";
        }

        private void MensajeRecebido(IAsyncResult aResult)
        {
            try
            {
                int size = cliente.EndReceiveFrom(aResult, ref endPointRemota);
                if (size > 0)
                {
                    byte[] mensajeRecibido = new byte[1464];
                    mensajeRecibido=(byte[]) aResult.AsyncState;

                    ASCIIEncoding encoding = new ASCIIEncoding();
                    string mensajeRecibidoString = encoding.GetString(mensajeRecibido);

                    /*Label mensajeCompa = new Label();
                    mensajeCompa.AutoSize = true;
                    mensajeCompa.BackColor = Color.White;
                    mensajeCompa.Font = new Font("Microsoft Sans Serif", 14.25F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
                    mensajeCompa.Location = new Point(3, 311);
                    mensajeCompa.Name = "label1";
                    mensajeCompa.Size = new Size(91, 48);
                    mensajeCompa.TabIndex = 0;
                    mensajeCompa.Text = Form1.textBox1.Text+mensajeRecibidoString;*/

                    listBox1.Items.Add("Amigo: " + mensajeRecibidoString);
                }

                byte[] buffer = new byte[1500];
                cliente.BeginReceiveFrom(buffer, 0, buffer.Length, SocketFlags.None, ref endPointRemota, new AsyncCallback(MensajeRecebido), buffer);

            }catch(Exception exp)
            {
                MessageBox.Show(exp.ToString());
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
                byte[] msg = new byte[1500];
                msg = enc.GetBytes(textBox1.Text);

                cliente.Send(msg);
                /*Label mensajeCompa = new Label();
                mensajeCompa.AutoSize = true;
                mensajeCompa.BackColor = Color.White;
                mensajeCompa.Font = new Font("Microsoft Sans Serif", 14.25F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
                mensajeCompa.Location = new Point(3, 311);
                mensajeCompa.Name = "label1";
                mensajeCompa.Size = new Size(91, 48);
                mensajeCompa.TabIndex = 0;
                mensajeCompa.Text = Form1.textBox1.Text + textBox1.Text;*/
                listBox1.Items.Add("Yo: " + textBox1.Text);
                textBox1.Clear();

                
            }
            catch (Exception ex)
            {
                
            }
        }

        private void chat_Load(object sender, EventArgs e)
        {
            string rutaImg = Form1.imagen;
            pictureBox1.Image = Image.FromFile(rutaImg);

            lblNombre.Text = Form1.nombre;


            endPointLocal = new IPEndPoint(IPAddress.Parse("127.0.0.1"), Form1.puerto);
            cliente.Bind(endPointLocal);

            endPointRemota = new IPEndPoint(IPAddress.Parse("127.0.0.1"), Form1.puertoDestino);
            cliente.Connect(endPointRemota);

            byte[] buffer = new byte[1500];
            cliente.BeginReceiveFrom(buffer, 0, buffer.Length, SocketFlags.None, ref endPointRemota, new AsyncCallback(MensajeRecebido), buffer);
        }
    }
}
