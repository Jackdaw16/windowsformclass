﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gallery
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            for(int i=0; i<imageList1.Images.Count; i++)
            {
                Label cuadroImagen = new Label();
                cuadroImagen.AutoSize = false;
                cuadroImagen.Text = "";
                cuadroImagen.ImageList = imageList1;
                cuadroImagen.Size= new Size(100, 100);
                cuadroImagen.ImageIndex = i;

                flowLayoutPanel1.Controls.Add(cuadroImagen);
                flowLayoutPanel1.Refresh();
                this.Refresh();
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            listBox1.Items.Add(textBox1.Text.ToString());
        }

        private void button3_Click(object sender, EventArgs e)
        {
            listBox1.Items.Remove(textBox1.Text.ToString());
        }
    }
}
