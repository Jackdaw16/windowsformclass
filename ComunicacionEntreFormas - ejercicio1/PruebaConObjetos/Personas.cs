﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaConObjetos
{
    class Personas
    {
        private String dni;
        private String nombre;
        private int edad;

        public Personas(String dni, String nombre, int edad)
        {
            this.dni = dni;
            this.nombre = nombre;
            this.edad = edad;
        }

        public string Dni { get => dni; set => dni = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public int Edad { get => edad; set => edad = value; }

    }

}
